.DEFAULT_GOAL := help
.SILENT:
.PHONY: vendor

## Colors
COLOR_RESET   = \033[0m
COLOR_INFO    = \033[32m
COLOR_COMMENT = \033[33m

## Help
help:
	printf "${COLOR_COMMENT}Usage:${COLOR_RESET}\n"
	printf " make [target]\n\n"
	printf "${COLOR_COMMENT}Available targets:${COLOR_RESET}\n"
	awk '/^[a-zA-Z\-\_0-9\.@]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf " ${COLOR_INFO}%-16s${COLOR_RESET} %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

define exec
	docker-compose exec app $(1) 2> /dev/null || $(1)
endef

define run
	docker-compose run --rm app $(1) 2> /dev/null || $(1)
endef

ifneq (,$(findstring sonar,$(MAKECMDGOALS)))
phpunit_options := $(phpunit_options) --coverage-clover build/reports/coverage.xml --log-junit build/reports/tests.xml
endif

##################
# Useful targets #
##################

## Install all install_* requirements and launch project.
install: env_file env_run install_vendor #install_db

## Run project, install vendors and run migrations.
run: env_run install_vendor #install_db

## Stop project.
stop:
	docker-compose stop

## Down project and remove volumes (databases).
down:
	docker-compose down -v --remove-orphans

## Run all quality assurance tools (tests and code inspection).
qa: code_static_analysis code_fixer code_detect code_correct clear_test_cache test_spec test 

########
# Code #
########

## Run codesniffer to correct violations of a defined coding project standards.
code_correct:
	$(call exec, bin/phpcs --standard=PSR2 src)

## Run codesniffer to detect violations of a defined coding project standards.
code_detect:
	$(call exec, bin/phpcbf --standard=PSR2 src tests)

## Run cs-fixer to fix php code to follow project standards.
code_fixer:
	$(call exec, bin/php-cs-fixer fix)

## Run PHPStan to find errors in code.
code_static_analysis:
	$(call exec, bin/phpstan analyse src --level max)

## Sanity check
code_sanity_check:
	$(call exec, php ./bin/phpcpd src)
	$(call exec, php ./bin/phploc src)
	make code_correct

###############
# Environment #
###############

## Set defaut environment variables by copying env.dist file as .env.
env_file:
	cp .env.dist .env

## Launch docker environment.
env_run:
	docker-compose up -d --build

###########
# Install #
###########

## Run database migration.
install_db:
	$(call run, bin/console doctrine:migrations:migrate -n --allow-no-migration --all-or-nothing)

## Run test database migration.
install_db_test:
	$(call exec, bin/console doctrine:migrations:migrate -n --allow-no-migration --all-or-nothing --env=test)

## Install vendors.
install_vendor:
	$(call run, composer install --prefer-dist --no-scripts --no-progress --no-suggest)


#########
# Cache #
#########

## clear cache
clear_cache:
	$(call exec, bin/console cache:clear)

clear_test_cache:
	$(call exec, bin/console cache:clear --env=test)

clear_dev_cache:
	$(call exec, bin/console cache:clear --env=dev)

clear_prod_cache:
	$(call exec, bin/console cache:clear --env=prod)

########
# Test#
########

## Run unit&integration tests with pre-installing test database.
#test: install_db_test test_unit
test: test_unit

## Run unit&integration tests.
test_unit:
	$(call exec, bin/phpunit $(phpunit_options))

## Run php spect tests.
test_spec:
	$(call exec, bin/phpspec run)

install_prod_vendor:
	$(call exec, composer install --prefer-dist --no-dev --no-scripts --no-progress --no-suggest --classmap-authoritative --no-interaction)

assets_install:
	$(call exec, ./bin/console assets:install)
