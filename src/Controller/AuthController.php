<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthController extends AbstractController
{
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $password = $request->request->get('password');

        $data = [];
        $data['username'] = $request->request->get('userName');
        $data['firstName'] = $request->request->get('firstName');
        $data['lastName'] = $request->request->get('lastName');
        $data['gender'] = $request->request->get('gender');
        $data['mobileNumber'] = $request->request->get('mobileNumber');
        $data['flatNumber'] = $request->request->get('flatNumber');

        $user = User::fromArray($data);
        $user->setPassword($encoder->encodePassword($user, $password));

        $em->persist($user);
        $em->flush();

        return new Response(sprintf('User %s successfully created', $user->getUsername()));
    }

    public function api()
    {
        return new Response(sprintf('Logged in as %s', $this->getUser()->getUsername()));
    }
}
