<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     itemOperations={"get"},
 *     collectionOperations={"get", "post"},
 *     normalizationContext={"datetime_format"="Y-m-d\TH:i:s", "groups"={"read"}},
 *     denormalizationContext={"datetime_format"="Y-m-d\TH:i:s", "groups"={"write"}}
 * )
 *
 * A communityhall.
 *
 * @ORM\Entity
 */
class CommunityHall
{
    /**
     * @var int id
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string $purpose
     * @Groups({"read", "write"})
     *
     * @ORM\Column(length=50)
     */
    public $purpose;

    /**
     * @var \DateTimeInterface startDate
     * @Groups({"read", "write"})
     *
     * @ORM\Column(type="datetime")
     */
    public $startDate;

    /**
     * @var \DateTimeInterface endDate
     * @Groups({"read", "write"})
     *
     * @ORM\Column(type="datetime")
     */
    public $endDate;

    /**
     * @var boolean
     * @Groups({"read"})
     *
     * @ORM\Column(type="boolean")
     */
    public $approved;

    public function getId(): ?int
    {
        return $this->id;
    }
}