<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     itemOperations={"get"},
 *     collectionOperations={"get", "post"},
 *     normalizationContext={"datetime_format"="Y-m-d\TH:i:s", "groups"={"read"}},
 *     denormalizationContext={"datetime_format"="Y-m-d\TH:i:s", "groups"={"write"}}
 * )
 *
 * A user.
 *
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
final class User implements UserInterface
{
    /**
     * @var int id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $password;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var string firstName
     * @Groups({"read", "write"})
     *
     * @ORM\Column(length=50)
     */
    public $firstName;

    /**
     * @var string lastName
     * @Groups({"read", "write"})
     *
     * @ORM\Column(length=50)
     */
    public $lastName;

    /**
     * @var string gender
     * @Groups({"read", "write"})
     *
     * @ORM\Column(type="string", length=1)
     */
    public $gender;

    /**
     * @var string mobileNumber
     * @Groups({"read", "write"})
     *
     * @ORM\Column(type="string", length=14)
     */
    public $mobileNumber;

    /**
     * @var int flatNumber
     * @Groups({"read", "write"})
     *
     * @ORM\Column(type="smallint", length=3)
     */
    public $flatNumber;

    public function __construct()
    {
    }

    public static function fromArray(array $data): self
    {
        $user = new self();
        $user->isActive = true;
        $user->username = $data['username'];

        $user->firstName = $data['firstName'];
        $user->lastName = $data['lastName'];
        $user->gender = $data['gender'];
        $user->mobileNumber = $data['mobileNumber'];
        $user->flatNumber = $data['flatNumber'];

        return $user;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getusername()
    {
        return $this->username;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}